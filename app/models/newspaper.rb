class Newspaper < ActiveRecord::Base
  attr_accessible :title, :editor, :editor_id

  validates :title, :editor, presence: true
  validates :title, uniqueness: true


end
