require 'spec_helper'

describe Newspaper do

  it "must have a title" do
    FactoryGirl.build(:newspaper, title: nil).should_not be_valid
  end

  it "must have an editor" do
    FactoryGirl.build(:newspaper, editor: nil).should_not be_valid
  end

  it "passes with title and editor filled in" do
    FactoryGirl.build(:newspaper, title: "Techmeme", editor: "Charles").should be_valid
  end

  it "does not pass if it is missing a title or editor" do
    FactoryGirl.build(:newspaper, title: "WSJ", editor: nil).should_not be_valid
    FactoryGirl.build(:newspaper, title: nil, editor: "Bob").should_not be_valid
  end

  it "validates uniqueness of title" do
    FactoryGirl.create(:newspaper, title: "Techmeme", editor: "John").should be_valid
    FactoryGirl.build(:newspaper, title: "Techmeme", editor: "Bob").should_not be_valid
  end

  it "does not validate the uniqueness of editor" do
    FactoryGirl.create(:newspaper, title: "Techmeme", editor: "Dave").should be_valid
    FactoryGirl.build(:newspaper, title: "Mashable", editor: "Dave").should be_valid
  end

  context ""

end
